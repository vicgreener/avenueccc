package AvenueCode.CodingChallenge;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class US2_5484 {

		static WebDriver driver;
		
		public static void main(String[] args) throws InterruptedException {
			
			Logger logger = Logger.getLogger("");

	        logger.setLevel(Level.OFF);

	        String driverPath = "";

	        String url = "https://qa-test.avenuecode.com/";
	        String email_address = "vicgreener@outlook.com";

	        String password = "********";
			
	        driverPath = "/Users/vicgreener/eclipse-workspace1/chromedriver";
	        System.setProperty("webdriver.chrome.driver", driverPath);

	        System.setProperty("webdriver.chrome.silentOutput", "true");

	        

	        driver = new ChromeDriver();

	        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

	        driver.get(url);
	        
	        driver.findElement(By.xpath("/html/body/div[1]/div[1]/div/div[2]/ul[2]/li[1]/a")).click();
	        driver.findElement(By.id("user_email")).sendKeys(email_address);
	        driver.findElement(By.id("user_password")).sendKeys(password);
	        
	        driver.findElement(By.xpath("//*[@id=\"new_user\"]/input")).click();
	        
	        driver.findElement(By.xpath("/html/body/div[1]/div[3]/center/a")).click();
		
	        //click "+" button
	        driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/form/div[2]/span")).click();

	        //click "Manage SubTask" button
	        driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr/td[4]/button")).click();
	        
	        //click "+Add" button
	        driver.findElement(By.id("add-subtask")).click();
	        
	        String ActualError = driver.findElement(By.xpath("/html/body/div[4]/div/div/div[2]/div[2]/h4")).getText();
	        String ExpectedError = "The field is required";
	        
	        if(ActualError.equals(ExpectedError))
	        {
	        	System.out.println("The actual and expected results are same. The error is " + ActualError);
	        }
	       else
	       {
	        	System.out.println("The actual and expected results are not same. The error message is missed");
	        }
	        
	}

}
