package AvenueCode.CodingChallenge;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class US1_5483 {

	
	static WebDriver driver;
	
	public static void main(String[] args) throws InterruptedException {
		
		Logger logger = Logger.getLogger("");

        logger.setLevel(Level.OFF);

        String driverPath = "";

        String url = "https://qa-test.avenuecode.com/";
        String email_address = "vicgreener@outlook.com";

        String password = "*********";
		
        driverPath = "/Users/vicgreener/eclipse-workspace1/chromedriver";
        System.setProperty("webdriver.chrome.driver", driverPath);

        System.setProperty("webdriver.chrome.silentOutput", "true");

        

        driver = new ChromeDriver();

        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

        driver.get(url);
        
        driver.findElement(By.xpath("/html/body/div[1]/div[1]/div/div[2]/ul[2]/li[1]/a")).click();
        driver.findElement(By.id("user_email")).sendKeys(email_address);
        driver.findElement(By.id("user_password")).sendKeys(password);
        
        driver.findElement(By.xpath("//*[@id=\"new_user\"]/input")).click();
        
        driver.findElement(By.xpath("/html/body/div[1]/div[3]/center/a")).click();
        
        String ExpectedMessage = "Hey Victor Greener, this is your todo list for today:";
        String ActualMessage = driver.findElement(By.xpath("/html/body/div[1]/h1")).getText();
        
        if(ActualMessage.equals(ExpectedMessage))
        {
        	System.out.println("The actual and expected results are same. The message is " + ActualMessage);
        }
       else
       {
        	System.out.println("The actual and expected results are not same. The message is " + ActualMessage);
        }
        
        driver.quit();
        
	}

}
